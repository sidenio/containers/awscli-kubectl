FROM debian:12-slim
LABEL maintainer="Siden <docker@siden.io>"

RUN \
  apt update && \
  apt install -y \
    curl \
    git \
    unzip \
    && \
  apt autoremove -y && \
  apt clean

COPY .bashrc /root/.bashrc
COPY .tool-versions /root/.tool-versions

RUN git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.12.0
RUN bash -lc "cut -d' ' -f1 ~/.tool-versions | xargs -n1 asdf plugin add"
RUN bash -lc "asdf install"

ENTRYPOINT ["/bin/bash", "-lc"]
